# Importamos el módulo os para generar el path a la página de login
import os
# importamos la interfaz que nos va a permitir simular la navegación con un navegador
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver import ActionChains


# Crear la url de la página de login
url_login = "file://" + os.getcwd() + "/files/login.html"

# Crear una sesión de Chrome
driver = webdriver.Chrome()
driver.maximize_window()

# Acceder a la página login
driver.get(url_login)

# Parada para ver el la página login
sleep(5)

# Busco el botón submit por su ID
element = driver.find_element(By.ID,"submit")

# Indico un clic sobre el elemento
element.click()

sleep(3)
# Ahora tengo que ubicar desplegar el menú Tablas
# Sé que es class="menu1"
# Recupero todos los elementos class="menu1"
menus = driver.find_elements(By.CLASS_NAME,"menu1")
for item in menus:
    if(item.text == "Tablas"):
        # Cuando el texto es el indicado, simulo el mouse over
        # Esto es necesario porque de lo contrario no me encontraría
        # el link Productos porque está oculto
        ActionChains(driver).move_to_element(item).perform()
        break
sleep(3)

# Ahora con el menú desplegado recupero todos los links en la página
menus = driver.find_elements(By.TAG_NAME,"a")
for item in menus:
    if(item.text == "Productos"):
        # Cuando el texto es el indicado, simulo el mouse over
        ActionChains(driver).move_to_element(item).perform()
        # Hago clic en el link
        item.click()
        break
# Recupero el botón Aceptar
aceptar = driver.find_element(By.ID,"aceptar")
# Hago click
aceptar.click();
# Recupero el div de mensajes
message = driver.find_element(By.ID,"globalMessage");
sleep(4)
# Verifico que el error de código vacío aparece en pantalla
error = ("El código no puede quedar vacío" in message.text)
if(not error):
    print("""FALLO EL TEST!!!
             PERMITE CONTINUAR SIN CODIGO DE PRODUCTO....""")
    # Cierro el navegador
    driver.close()
    driver.quit()
# Recupero el input ID inputCode
inputCode = driver.find_element(By.ID,"inputCode")
# Escribo un código alfanumérico
inputCode.send_keys("TEST0001")
# Recupero el botón Aceptar
aceptar = driver.find_element(By.ID,"aceptar")
# Hago click
aceptar.click();
# Recupero el div de mensajes
message = driver.find_element(By.ID,"globalMessage");
sleep(4)
error = ("El código no cumple el formato" in message.text)
if(not error):
    print("""FALLO EL TEST!!!
             PERMITE CONTINUAR SIN CODIGO DE PRODUCTO....""")
    # Cierro el navegador
    driver.quit()

# Recupero el select
combo = driver.find_element(By.ID,"selectRubro")
# Hago foco sobre el mismo
ActionChains(driver).move_to_element(combo).perform()
# Hago click en el combo para desplegar la lista
combo.click()
# Entre las lista busco el que me interesa
for option in combo.find_elements(By.TAG_NAME,'option'):
    if option.text == "Electronica":
        # Hago click para seleccionarlo
        option.click()
        break
    sleep(1)

# Si ha encontrado todos los errores finaliza correctamente
print("-=[ EL TEST FINALIZO CORRECTAMENTE ]=-")
# driver.quit()
