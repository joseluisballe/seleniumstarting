# Importamos el módulo os para generar el path a la página de login
from datetime import datetime
import os
# importamos la interfaz que nos va a permitir simular la navegación con un navegador
from selenium import webdriver
from selenium.webdriver.common.by import By
from time import sleep
from selenium.webdriver import ActionChains

def resaltar(element):
    """Highlights (blinks) a Selenium Webdriver element"""
    #https://stackoverflow.com/questions/52207164/python-selenium-highlight-element-doesnt-do-anything
    effect_time=2
    color="red"
    border=4
    xdriver = element._parent
    def apply_style(s):
        xdriver.execute_script("arguments[0].setAttribute('style', arguments[1]);",
                              element, s)
    original_style = element.get_attribute('style')
    apply_style("border: {0}px solid {1};".format(border, color))
    sleep(effect_time)
    apply_style(original_style)


def get_screenshot_file_name():
    return os.getcwd()+"/screenshot/"+datetime.today().strftime("%Y%m%d%H%M%S")+".test.png"


def element_escribir_by(by:By,selector:str,texto:str):
    element = driver.find_element(by,selector)
    resaltar(element)
    element.clear()
    webdriver.ActionChains(driver).move_to_element(element).perform()
    element.send_keys(texto)
    return element


def element_click_by(by:By, objId:str):
    element = driver.find_element(by,objId)
    resaltar(element)
    webdriver.ActionChains(driver).move_to_element_with_offset(element,5,5).perform()
    element.click()
    return element

def verificar_globalMessages(msg:str):
    element = driver.find_element(By.ID,"globalMessage")
    error = (msg in element.text)
    if( not error):
        quit_with_message("FALLO EL TEST: " + msg);

def imprimir_pantalla():
    archivo = get_screenshot_file_name()
    driver.save_screenshot(archivo)
    return archivo;
    
def quit_with_message(msg:str):
    print("## TEST INFO")
    print("## " + msg)
    pantalla = imprimir_pantalla()
    print("Screenshot at: " + pantalla)
    driver.quit


def combo_select_option_by_id(id,visibleText):
    combo = driver.find_element(By.ID,id)
    webdriver.ActionChains(driver).move_to_element(combo).perform()
    combo.click()
    for option in combo.find_elements(By.TAG_NAME,'option'):
        if option.text == visibleText:
            option.click() # select() in earlier versions of webdriver
            break    
    sleep(2)
    return combo

# Armo la url de la página de login
url_login = "file://" + os.getcwd() + "/files/login.html"

# Crear una sesión de Chrome
driver = webdriver.Chrome()

# driver.implicitly_wait(10)
driver.maximize_window()

# Acceder a la página login
driver.get(url_login)

# Parada para ver el la página login
sleep(3)

element_click_by(By.ID,"submit")

sleep(3)
# Ahora tengo que ubicar desplegar el menú Tablas
# Sé que es class="menu1"
# Recupero todos los elementos class="menu1"
menus = driver.find_elements(By.CLASS_NAME,"menu1")
for item in menus:
    if(item.text == "Tablas"):
        # Cuando el texto es el indicado, simulo el mouse over
        # Esto es necesario porque de lo contrario no me encontraría
        # el link Productos porque está oculto
        ActionChains(driver).move_to_element(item).perform()
        break
sleep(3)

# Ahora con el menú desplegado recupero todos los links en la página
menus = driver.find_elements(By.TAG_NAME,"a")
for item in menus:
    if(item.text == "Productos"):
        # Cuando el texto es el indicado, simulo el mouse over
        ActionChains(driver).move_to_element(item).perform()
        # Hago clic en el link
        item.click()
        break
# Formulario vacío.
# Click en Aceptar
element_click_by(By.ID,"aceptar")
# Verifico que el error de código vacío aparece en pantalla
verificar_globalMessages("El código no puede quedar vacío")
sleep(2)

# Ingresar un código con formato incorrecto
element_escribir_by(By.ID,"inputCode","TEST0001")
# Click en Aceptar
element_click_by(By.ID,"aceptar")
# Verifico que el error de formato incorrecto aparece en pantalla
verificar_globalMessages("El código no cumple el formato")
sleep(2)

# Ingresar un código correcto pero el nombre vacío
element_escribir_by(By.ID,"inputCode","111-12345678")
# Click en Aceptar
element_click_by(By.ID,"aceptar")
# Verifico que el error de nombre vacío
verificar_globalMessages("El nombre no puede quedar vacío")
sleep(2)

# Ingresar el nombre rubro vacío
element_escribir_by(By.ID,"inputName","PRODUCTO TEST")
# Click en Aceptar
element_click_by(By.ID,"aceptar")
# Verifico que el error de rubro vacío
verificar_globalMessages("Debe seleccionar un rubro")
sleep(2)

# Elegir el rubro pero sin precio.
combo_select_option_by_id("selectRubro","Electronica")
# Click en Aceptar
element_click_by(By.ID,"aceptar")
# Verifico que el error de precio vacío
verificar_globalMessages("El precio no puede quedar vacío")
sleep(2)

# Ingresar el precio
element_escribir_by(By.ID,"inputPrecio","120")
# Click en Aceptar
element_click_by(By.ID,"aceptar")
# Verifico que el mensaje de confirmación
verificar_globalMessages("El producto se ingresó correctamente")
sleep(2)

# Si ha encontrado todos los errores finaliza correctamente
pantalla = imprimir_pantalla()
print("-=[ EL TEST FINALIZO CORRECTAMENTE ]=-")
print("Screenshot: " + pantalla)
driver.quit()